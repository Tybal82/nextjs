import { useState } from 'react'

const NavBar = props => {
     

    return (
        <section>

           
            <style jsx>{`
            @media (min-width : 0px) {
                section {
                    position: fixed;
                    top: 0;
                    left: 0;
                    display: flex;
                    justify-content: space-evenly;
                    alignItems: center;
                    width: ${props.navBarDisplay === false ? '0' : '200px'};
                    height: 100vh;
                    background: var(--lightgrey);
                    transition: width .2s;
                }
            }
            @media (min-width : 1024px) {
                section {
                    position: fixed;
                    top: 0;
                    left: 0;
                    display: flex;
                    justify-content: space-evenly;
                    alignItems: center;
                    width: 100vw;
                    height: ${props.navBarDisplay === false ? '0' : '100px'};
                    background: var(--lightgrey);
                    transition: height .2s;
                }
            }
            `}</style>
        </section>

    )
}





export default NavBar
