import { useState } from 'react'
import S from '../Segment'
const NavBox = () => {
    // STATES
    const [navBarDisplay, setNavBarDispaly] = useState(false)
    // HANDLERS
    const handleNavBarDisplay = () => {
        setNavBarDispaly(!navBarDisplay)
    }
    return (
        <nav>
            <S.NavBar
                navBarDisplay={navBarDisplay}
            />
            <S.NavIcon
                handleNavBarDisplay={handleNavBarDisplay}
                navBarDisplay = {navBarDisplay}
            />
        </nav>
    )
}

export default NavBox
