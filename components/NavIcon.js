// import navIcon0 from '../static/img/navIcon-0.svg'

const NavIcon = props => {
    return (
        <div onClick={props.handleNavBarDisplay}>
            <img src='../static/img/navIcon-0.svg'></img>
            <style jsx>{`
             @media (min-width : 0px) {
                div {
                    position: fixed;
                    top: 10px;
                    left: ${props.navBarDisplay === true ? '87.5px' : '10px'};
                    width: 25px;
                    cursor: pointer;
                    transition: all .2s;
                }
            }
            @media (min-width : 1024px) {
                div {
                    position: fixed;
                    top: ${props.navBarDisplay === true ? '37.5px' : '10px'};
                    left: 10px;
                    width: 25px;
                    cursor: pointer;
                    transition: all .2s;
                }
            }
      `}</style>
        </div>
    )
}

export default NavIcon
