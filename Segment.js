import Head from 'next/head'
import NavBox from './components/NavBox'
import NavBar from './components/NavBar'
import NavIcon from './components/NavIcon'
const S = {
    Head,
    NavBox,
    NavBar,
    NavIcon
}

export default S